const text = document.querySelectorAll(".text");
const container = document.querySelector(".container");


window.addEventListener('keypress', myFun);

function myFun(e) {
    text.forEach(function (element, index, arr) {
        if (e.charCode == 114) {
            element.style.color = "red";
            container.style.borderColor = "red";
        }
        else if (e.charCode == 103) {
            element.style.color = "green";
            container.style.borderColor = "green";
        }
        else if (e.charCode == 98) {
            element.style.color = "blue";
            container.style.borderColor = "blue";
        }
        else {
            element.style.color = "grey";
            container.style.borderColor = "#c0c0c0";
        };
    });
};

