const main = document.getElementById("main");


window.addEventListener('keydown', myFunction);

function myFunction(e) {
    e.preventDefault();
    if (e.ctrlKey && e.keyCode == 83 && e.shiftKey == false) {
        print("Ctrl + S = save");
    }
    else if (e.ctrlKey && e.keyCode == 65) {
       print("Ctrl + A = Everything is selected")
    }
    else if (e.ctrlKey && e.shiftKey && e.keyCode == 83) {
      print("Ctrl + Shift + S = All saved")
    }
};


function print(message) {
    document.getElementById("text").innerHTML += message + "<br />";
}