const obj = document.querySelector(".banner__info")

const minDistance = 270;

// document.documentElement.clientWidth - ширина екрану
const maxX = document.documentElement.clientWidth - 210;
const maxY = document.documentElement.clientHeight - 210;

document.addEventListener('mousemove', function (evt) {
    // getBoundingClientRect - координати елемента, з яким працюємо
    const objCoords = obj.getBoundingClientRect();
    // clientX, clientY - координати мишки при виклику події
    const distance = calcDistance(evt.clientX, evt.clientY, objCoords.x, objCoords.y);
    if (distance < minDistance) {
        obj.style.left = getRandomInt(0, maxX) + 'px';
        obj.style.top = getRandomInt(0, maxY) + 'px';
    }
});

function calcDistance(x1, y1, x2, y2) {
    return Math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2);
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
